#-------------------------------------------------
#
# Project created by QtCreator 2019-01-22T10:05:36
#
#-------------------------------------------------

QT       += core gui widgets printsupport

TARGET = PlotIt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG( release, debug|release ) {
    DESTDIR = $${_PRO_FILE_PWD_}/bin
    win32 {
        DEPLOY_COMMAND = windeployqt
        DEPLOY_TARGET = $$shell_quote($$shell_path($${DESTDIR}))
        QMAKE_POST_LINK += $${DEPLOY_COMMAND} $${DEPLOY_TARGET}
    }
}

INCLUDEPATH += $$PWD/ext

SOURCES += \
        src/main.cpp \
        src/MainWindow.cpp \
        src/MainWindow.cpp \
        ext/qcustomplot.cpp \
    src/PlotSettingsDlg.cpp \
    src/DoubleReaderProvider.cpp \
    src/FloatReaderProvider.cpp

HEADERS += \
        src/MainWindow.h \
        src/MainWindow.h \
        ext/qcustomplot.h \
    src/PlotSettingsDlg.h \
    src/Reader.h \
    src/SimpleReader.h \
    src/ReaderProvider.h \
    src/DoubleReaderProvider.h \
    src/FloatReaderProvider.h \
    src/WindowBuilder.h

