#include "MainWindow.h"

#include <QDragEnterEvent>
#include <QDropEvent>
#include <QFileInfo>
#include <QShortcut>
#include <random>
#include <memory>

#include "WindowBuilder.h"
#include "PlotSettingsDlg.h"

MainWindow::MainWindow(QWidget *parent)
	: QCustomPlot(parent)
{
	setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
	xAxis2->setVisible(true);
	xAxis2->setTickLabels(false);
	yAxis2->setVisible(true);
	yAxis2->setTickLabels(false);
	// make left and bottom axes always transfer their ranges to right and top axes:
	connect(xAxis, static_cast<void(QCPAxis::*)(const QCPRange&)>(&QCPAxis::rangeChanged),
		xAxis2, static_cast<void(QCPAxis::*)(const QCPRange&)>(&QCPAxis::setRange));
	connect(yAxis, static_cast<void(QCPAxis::*)(const QCPRange&)>(&QCPAxis::rangeChanged),
		yAxis, static_cast<void(QCPAxis::*)(const QCPRange&)>(&QCPAxis::setRange));

	QShortcut *clearShortcut = new QShortcut(QKeySequence(QKeySequence::Delete), this);
	connect(clearShortcut, &QShortcut::activated, this, &MainWindow::clear);

	QShortcut *reloadShortcut = new QShortcut(QKeySequence(Qt::Key_F5), this);
	connect(reloadShortcut, &QShortcut::activated, this, &MainWindow::reload);

	QShortcut *rescaleShortcut = new QShortcut(QKeySequence(Qt::Key_F8), this);
	connect(rescaleShortcut, &QShortcut::activated, this, &MainWindow::rescale);

	setWindowTitle("PlotIt");
	setAcceptDrops(true);
	resize(320, 240);
}

bool MainWindow::load(const std::shared_ptr<Reader> &reader)
{
	const bool res = loadPrivate(reader);
	if (res)
	{
		_readers << reader;
	}
	return res;
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
	if (event->mimeData()->hasUrls())
	{
		event->acceptProposedAction();
	}
}

void MainWindow::dropEvent(QDropEvent *event)
{
	PlotSettingsDlg dlg(this);
	if (dlg.exec() != QDialog::Accepted)
	{
		event->ignore();
		return;
	}
	const auto fileList = event->mimeData()->urls();
	const PlotSettingsDlg::WidgetType type = dlg.widgetType();
	MainWindow *widget = nullptr;
	for (const auto &file : fileList)
	{
		const QString fileName = file.toLocalFile();
		std::shared_ptr<Reader> reader(dlg.reader());
		reader->setFileName(fileName);
		switch (type)
		{
		case PlotSettingsDlg::Same:
			widget = this;
			break;
		case PlotSettingsDlg::NewOne:
			if (widget == nullptr)
			{
				widget = WindowBuilder<MainWindow>::create();
			}
			break;
		case PlotSettingsDlg::NewOnes:
			widget = WindowBuilder<MainWindow>::create();
			break;
		}
		widget->load(reader);
	}
	event->acceptProposedAction();
}

bool MainWindow::loadPrivate(const std::shared_ptr<Reader> &reader)
{
	if (!reader->isOpen() && !reader->open())
	{
		return false;
	}

	const bool res = reader->read();
	if (res)
	{
		plot(QFileInfo(reader->fileName()).fileName(), reader->x(), reader->y());
	}
	reader->reset();
	reader->close();
	return res;
}

void MainWindow::plot(const QString &title, const QVector<double> &x, const QVector<double> &y)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> colorDis(0, 0x1000000);
	std::uniform_int_distribution<> styleDis(QCPScatterStyle::ssCross, QCPScatterStyle::ssPeace);

	const QColor color = QColor::fromRgb(colorDis(gen));
	const QCPScatterStyle::ScatterShape tickStyle = QCPScatterStyle::ScatterShape(styleDis(gen));

	QCPGraph *graph = addGraph();
	graph->setPen(QPen(color, 0.5));
	graph->setScatterStyle(tickStyle);

	graph->setData(x, y);
	graph->setName(title);
	legend->setVisible(true);

	rescale();
}

void MainWindow::reload()
{
	clearGraphs();
	for (const auto &reader : _readers)
	{
		loadPrivate(reader);
	}
}

void MainWindow::clear()
{
	_readers.clear();
	clearGraphs();
	legend->setVisible(false);
	replot();
}

void MainWindow::rescale()
{
	rescaleAxes();
	replot();
}
