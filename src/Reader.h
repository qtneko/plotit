#pragma once
#include <QString>
#include <QVector>
#include <QFile>

class Reader
{
public:

	Reader() = default;
	virtual ~Reader(){}

	void setFileName(const QString &fileName)
	{
		_file.setFileName(fileName);
	}

	QString fileName() const
	{
		return _file.fileName();
	}

	bool open(const QString &fileName)
	{
		setFileName(fileName);
		return open();
	}

	bool open()
	{
		return _file.open(QIODevice::ReadOnly);
	}

	bool isOpen() const
	{
		return _file.isOpen();
	}

	void close()
	{
		_file.close();
	}

	virtual void reset() = 0;
	virtual bool read() = 0;
	virtual const QVector<double> &x() const = 0;
	virtual const QVector<double> &y() const = 0;

protected:
	QFile	_file;

};
