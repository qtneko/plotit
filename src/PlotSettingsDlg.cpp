#include "PlotSettingsDlg.h"
#include <QFormLayout>
#include <QComboBox>
#include <QDialogButtonBox>

#include "DoubleReaderProvider.h"
#include "FloatReaderProvider.h"

PlotSettingsDlg::PlotSettingsDlg(QWidget *parent)
	: QDialog(parent)
{
	_providers << (new DoubleReaderProvider) << (new FloatReaderProvider);

	QFormLayout *mainLayout = new QFormLayout;

	_widgetType = new QComboBox;
	_widgetType->addItem("same", Same);
	_widgetType->addItem("all in new one", NewOne);
	_widgetType->addItem("all in new ones", NewOnes);
	_widgetType->setCurrentIndex(0);
	mainLayout->addRow("Window:", _widgetType);

	_dataType = new QComboBox;
	for (const auto &provider : _providers)
	{
		_dataType->addItem(provider->typeName());
	}
	_dataType->setCurrentIndex(0);
	mainLayout->addRow("Data type:", _dataType);

	QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
													   | QDialogButtonBox::Cancel);

	connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
	connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
	mainLayout->addRow(buttonBox);

	setLayout(mainLayout);
	resize(sizeHint());
}

PlotSettingsDlg::~PlotSettingsDlg()
{
	qDeleteAll(_providers);
}

std::shared_ptr<Reader> PlotSettingsDlg::reader() const
{
	return _providers.at(_dataType->currentIndex())->provider();
}

PlotSettingsDlg::WidgetType PlotSettingsDlg::widgetType() const
{
	return WidgetType(_widgetType->currentData().toInt());
}
