#include <QApplication>
#include "MainWindow.h"
#include "WindowBuilder.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	WindowBuilder<MainWindow>::create();
	return a.exec();
}
