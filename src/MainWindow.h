#pragma once
#include <QString>
#include <QVector>
#include <memory>
#include "qcustomplot.h"
#include "Reader.h"

class QDragEnterEvent;
class QDropEvent;

class MainWindow : public QCustomPlot
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);

	bool load(const std::shared_ptr<Reader> &reader);

protected:
	void dragEnterEvent(QDragEnterEvent *event) override;
	void dropEvent(QDropEvent *event) override;

private:
	QVector<std::shared_ptr<Reader>>	_readers;

	bool loadPrivate(const std::shared_ptr<Reader> &reader);
	void plot(const QString &title, const QVector<double> &x, const QVector<double> &y);
	void reload();
	void clear();
	void rescale();

};

