#pragma once
#include "ReaderProvider.h"

class FloatReaderProvider : public ReaderProvider
{
public:
	QString typeName() const override;
	ReaderPtr provider() const override;
};
