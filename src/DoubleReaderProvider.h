#pragma once
#include "ReaderProvider.h"

class DoubleReaderProvider : public ReaderProvider
{
public:
	QString typeName() const override;
	ReaderPtr provider() const override;
};
