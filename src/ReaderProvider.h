#pragma once
#include <memory>
#include <QString>
#include "Reader.h"

class ReaderProvider
{
public:
	using ReaderPtr = std::shared_ptr<Reader>;

	ReaderProvider() {}
	virtual ~ReaderProvider() {}

	virtual QString typeName() const = 0;
	virtual ReaderPtr provider() const = 0;
};
