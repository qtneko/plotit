#pragma once
#include "Reader.h"

template<typename T>
class SimpleReader : public Reader
{
public:

	void reset() override
	{
		_x.clear();
		_y.clear();
	}

	bool read() override
	{
		if (!_file.isOpen())
		{
			return false;
		}

		const int size = static_cast<int>(_file.size() / sizeof(T));
		_y.reserve(size);
		T value;
		while (_file.read(reinterpret_cast<char *>(&value), sizeof(T)) == sizeof(T))
		{
			_y.push_back(static_cast<double>(value));
		}
		_x.resize(_y.size());
		std::iota(_x.begin(), _x.end(), 0.0);
		return true;
	}

	const QVector<double> &x() const override
	{
		return _x;
	}

	const QVector<double> &y() const override
	{
		return _y;
	}

private:
	QVector<double>	_x;
	QVector<double>	_y;

};
