#include "DoubleReaderProvider.h"
#include "SimpleReader.h"

using DoubleReader = SimpleReader<double>;

QString DoubleReaderProvider::typeName() const
{
	return "double";
}

DoubleReaderProvider::ReaderPtr DoubleReaderProvider::provider() const
{
	return ReaderPtr(new DoubleReader);
}
