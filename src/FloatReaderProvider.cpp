#include "FloatReaderProvider.h"
#include "SimpleReader.h"

using FloatReader = SimpleReader<float>;

QString FloatReaderProvider::typeName() const
{
	return "float";
}

FloatReaderProvider::ReaderPtr FloatReaderProvider::provider() const
{
	return ReaderPtr(new FloatReader);
}
