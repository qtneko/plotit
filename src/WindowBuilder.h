#pragma once
#include <QWidget>

template<typename T>
class WindowBuilder
{
public:
	WindowBuilder() = delete;

	static T *create()
	{
		T *window = new T;
		window->setAttribute(Qt::WA_DeleteOnClose);
		window->setWindowFlag(Qt::Window);
		window->show();
		return window;
	}
};
