#pragma once
#include <QDialog>
#include <QVector>
#include <memory>
#include "ReaderProvider.h"

class QComboBox;

class PlotSettingsDlg : public QDialog
{
public:
	enum WidgetType {Same, NewOne, NewOnes};

	explicit PlotSettingsDlg(QWidget *parent = nullptr);
	~PlotSettingsDlg();

	std::shared_ptr<Reader> reader() const;
	WidgetType widgetType() const;

private:
	QComboBox					*_widgetType = nullptr;
	QComboBox					*_dataType = nullptr;
	QVector<ReaderProvider *>	_providers;

};
